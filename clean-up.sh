#!/usr/bin/env bash
#
# Script name: clean-up.sh
# Description: Script for deleting everything not named "PKGBUILD".
# GitLab: https://www.gitlab.com/cabooshyy/cbsh-arch-pkgbuild
# Contributors: Cameron Miller

# Go into x86_64 sub-directory and get a list of files
# not named PKGBUILD and not named *.install.
find x86_64 -type f -not -name PKGBUILD -not -name "*.install" -delete
